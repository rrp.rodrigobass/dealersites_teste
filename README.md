# dealersites_teste

# **ESTE PROJETO FOI REALIZADO COM CODEIGNITER**

Para rodar o projeto local, você irá precisar instalar  um servidor PHP Apache com Mysql/ MariaDb.
Para isso, baixe o XAMPP ou o WAMP.

1 – Faça o download e depois instale o Xampp ou o WampServer;

2 – Faça o download de um editor de texto de sua preferência, caso não tenha um instalado;

3 – Faça o download do Git para controle de versionamento, para assim poder clonar o projeto em sua máquina;

# **Clonando o projeto**

4 – Após a instalação, navegue até a pasta www do seu WampServer ou a pasta htdocs do Xampp;

5 - Com o Git, basta fazer o clone do projeto na pasta.

Neste projeto foi utilizado o Xampp com Mysql, então irei abordar sobre ele.
Execute o Painel de controle do Xampp e inicialize os serviços do Apache e do Mysql;

Para vizalização do projeto acesse http://localhost/o_nome_da_pasta_do_projeto.

# **Banco de Dados**

Para importar o banco de dados utilizado, na pasta dump_database tem um arquivo sql para teste.


