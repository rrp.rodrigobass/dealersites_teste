<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function index()
	{
		$this->load->model("Users_model");
		$lista = $this->Users_model->buscaTodos();
		$numLogin = $this->Users_model->numeroLogin();
		$dados = array("users" => $lista, "login" => $numLogin);
		$this->load->view('user/list_users', $dados);
	}
}
