<?php
class Users_model extends CI_Model {

        public function __construct()
        {
                $this->load->database();
        }

        public function buscaTodos(){
                return $this->db->select("users.*, users_acess.last_login, users_acess.users_id")
                ->join("users_acess", "users_acess.users_id=users.id")
                ->get("users")->result_array();
        }
        public function numeroLogin(){
                return $this->db->select("users_acess.*, count(users_acess.users_id) as numLogin")
                ->get("users_acess")->result_array();
        }

        public function store($dados = null, $id = null) {
		
		if ($dados) {
			if ($id) {
				$this->db->where('id', $id);
				if ($this->db->update("users", $dados)) {
					return true;
				} else {
					return false;
				}
			} else {
				if ($this->db->insert("users", $dados)) {
					return true;
				} else {
					return false;
				}
			}
		}
		
        }
        
}