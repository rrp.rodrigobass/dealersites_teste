<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="./includes/bootstrap/css/style.css">
    <link rel="stylesheet" href="./includes/bootstrap/css/font.css">
    <link rel="stylesheet" href="./includes/bootstrap/css/bootstrap.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossorigin="anonymous">
    <script src="https://kit.fontawesome.com/77c8e73fb1.js" crossorigin="anonymous"></script>
<!-- Latest compiled and minified JavaScript -->
<script src="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js" integrity="sha384-aJ21OjlMXNL5UyIl/XNwTMqvzeRMZH2w8c5cRVpzpU8Y5bApTppSuUkhZXN0VxHd" crossorigin="anonymous"></script>
    
    <title>Lista de Usuários</title>

    <style>
        .align-self-start {
            align-self: auto!important;
        }
        .input-sm{
            margin-bottom: 10px!important;
        }
        @media (max-width: 767px) {
            .p-3 {
                padding: 3rem !important;
            }
        }
    </style>
</head>
<body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark justify-content-sm-start fixed-top" >
        <div class="container-fluid">
            <a class="navbar-brand order-1 order-lg-0 ml-lg-0 ml-2 mr-auto" href="#" >DealerSites - Teste</a>
            <button class="navbar-toggler align-self-start" type="button">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse bg-dark p-3 p-lg-0 mt-5 mt-lg-0 d-flex flex-column flex-lg-row flex-xl-row justify-content-lg-end mobileMenu" id="navbarSupportedContent" >
                <ul class="navbar-nav align-self-stretch">
                    <li class="nav-item">
                        <a class="nav-link" href="#">Cadastra Usuário <span class="sr-only">(current)</span></a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    
    <div class="overlay"></div>
    <div class="container py-4 my-5">
        <div class="row">
            <div class="col-md-12 ">
            <section class="wrapper">
                <div class="panel panel-default">
                    <!-- Default panel contents -->
                    <div class="panel-heading"><h1>Lista de Usuários</h1></div>
                    <div class="panel-body">
                        <form id="filtro_form" action="" method="post">
                            <div class="row">
                                <div class="col-md-4">
                                    <label></label>
                                    <section class="panel">
                                        <input placeholder="Nome:" class="form-control input-sm" value="<?php //echo $_POST["nome"] ?>" name="nome" />
                                    </section>
                                </div>
                                <div class="col-md-3">
                                    <label></label>
                                    <section class="panel">
                                        <input placeholder="E-mail:" class="form-control input-sm" type="text" value="<?php //echo $_POST["email"] ?>" name="email" />
                                    </section>
                                </div>
                                <div class="col-md-3">
                                    <label></label>
                                    <section class="panel">
                                        <input placeholder="Situação:" class="form-control input-sm" type="text" value="<?php //echo $_POST["situacao"] ?>" name="situacao" />
                                    </section>
                                </div>
                                <div class="col-md-2">
                                    <br />
                                    <section class="panel">
                                        <input type="hidden" name="filtrar" value="true" />
                                        <a href="#" onClick="document.getElementById('filtro_form').submit();">
                                            <i class="fa fa-search fa-2x btn_buscar" style="color:#FFFFFF;"></i>
                                        </a>
                                    </section>
                                </div>
                            </div>
                        </form>
                        <br />
                        <!-- Table -->
                        <table id="table_content" class="table table-responsive table-striped">
                            <thead>
                                <tr>
                                    <th style="width:30%">Nome</th>
                                    <th style="width:30%">Email</th>
                                    <th style="width:30%">Número de logins</th>
                                    <th style="width:10%">Situação</th>
                                    <th>Ações</th>
                                </tr>
                            </thead>
                            <?php foreach($users as $user){ ?>
                                <?php 
                                if($user["active"] === "1"){
                                    $active = "Ativo";
                                }else{ $active = "Inativo";} ?>
                                <tbody>
                                <tr data-expanded="true">
                                    <td><?php echo $user["name"]; ?></td>
                                    <td><?php echo $user["email"]; ?></td>
                                    <?php foreach($login as $log){ ?>
                                        <td><?php echo $log["numLogin"]; ?></td>
                                    <?php } ?>
                                    <td><?php echo $active; ?></td>
                                    <td class="col-md-2">
                                        <form action="" method="post" id="bloqueia_form_<?php echo $user["id"] ?>" style="margin-bottom: 0px;">
                                            <input type="hidden" name="bloquear" value="true" />
                                            <input type="hidden" name="status_ag" value="<?php echo $user["active"] == 0 ? 1 : 0 ?>" />
                                            <input type="hidden" name="id" value="<?php echo $user["id"] ?>" />
                                            <a href="#" onClick="document.getElementById('bloqueia_form_<?php echo $user["id"] ?>').submit();">
                                                <?php if ($user["active"] == 0) { ?>
                                                    <i class="fa fa-lock fa-2x" style="color: #a94442" title="Ativar">
                                                    </i>
                                                <?php } else { ?>
                                                    <i class="fa fa-lock-open fa-2x" style="color: #3c763d" title="Bloquear">
                                                    </i>
                                                <?php } ?>
                                            </a>
                                        </form>
                                    </td>
                                </tr>
                                </tbody>
                            <?php } ?>
                        </table>
                    </div>
                </div>
            </section>
            </div>
        </div>
    </div>
    <script
        src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"
        integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
        crossorigin="anonymous"
    ></script>

    <script src="./includes/bootstrap/js/script.js"></script>
</body>
</html>