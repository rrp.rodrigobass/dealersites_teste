FROM php:7.4-apache
COPY . /var/www/html/dealersites_teste/
RUN echo "ServerName localhost" >> /etc/apache2/apache2.conf
RUN apt update
# Install Mysql PDO
RUN apt-get install -y \
    && docker-php-ext-install pdo pdo_mysql
RUN a2enmod rewrite
RUN service apache2 restart

EXPOSE 80